import React from 'react'
import { useNavigate } from 'react-router-dom'
const EmpCard = ({ values }) => {
  const navigate = useNavigate();
  const {
    firstName,
    lastName,
    email,
    phonenumber, 
    userID
  } = values
  const handleClick = (e) => {
    e.preventDefault()
    navigate(`/admin/employee/${e.target.value}`)
    console.log('clicked')
  }
  return (
    <div className="grid grid-cols-2 gap-2 bg-[#44475a] text-white rounded-lg p-2">
      <div className='flex flex-col'>
        <p>First Name</p>
        <p>{firstName}</p>
      </div>
      <div className='flex flex-col'>
        <p>last name</p>
        <p>{lastName}</p>
      </div>
      <div className='flex flex-col'>
        <p>email</p>
        <p>{email}</p>
      </div>
      <div className='flex flex-col'>
        <p>Phone Number</p>
        <p>
          {phonenumber ? phonenumber : 'Not Provided'}
        </p>
      </div>
      <div className='flex justify-center col-span-2 items-center'>
        <button className='border-cyan-500 border rounded text-sm px-1 py-1'
          value={userID}
          onClick = {handleClick}
        >
          View Details
        </button>
      </div>
    </div>
  )
}

export default EmpCard
