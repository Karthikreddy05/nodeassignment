export default [
    {
      key: 'firstName',
      value: 'First Name',
    },
    {
      key: 'lastName',
      value: 'Last Name',
    },
    {
      key: 'skills',
      value: 'Skills',
    },
    {
      key: 'company',
      value: 'Company',
    },
    {
      key: 'food',
      value: 'Food Preference',
    },
    {
      key: 'interests',
      value: 'Interest',
    },
    {
      key: 'sports',
      value: 'Sports',
    },
  ]