import React, { useRef, useState } from 'react'
import EmpCardList from '../components/EmpCardList/EmpCardList'
import Axios from './../utils/Axios.js'
import SearchBar from '../components/SearchBar/SearchBar'
const AdminHome = () => {
  const searchValueState = useState({
     searchvalue : '',
      type : 'firstName'
  })
  const [searchValues] = searchValueState
  const [searchResults, setSearchResults] = useState([])
  const handleSearch = (e) => {
    e.preventDefault()
    if(searchValues.searchvalue !== ''){
    Axios.post('/admin/search', {
      ...searchValues
    }).then((response)=>{
      const results = response.data.data
      setSearchResults(results)
    })
    .catch((error)=>{
      console.log(error)
    })}
  }
  return (
    <>
      <div className="flex flex-col justify-center items-center">
        <div className="container flex justify-between my-4">
          <div className="text-white font-bold">
            <h1 className=" bg-gradient-to-r from-cyan-200 to-blue-700 text-transparent bg-clip-text animate-text text-4xl">
              EmpDB
            </h1>
          </div>
          <div className='flex justify-between w-44  '>
            <button className=" text-white rounded-md px-1 py-1 text-md font-bold hover:border hover:border-cyan-400 ">Search</button>
            <button className='mr-1 text-white font-bold rounded-md px-1 py-1 text-md hover:border hover:border-red-600'>Sign Out</button>
          </div>
        </div>
        <SearchBar  searchValueState={searchValueState} handleSearch= {handleSearch} />
        <EmpCardList searchResults = {searchResults} />
      </div>
    </>
  )
}

export default AdminHome
